# EdenUniverseBuilder v3.0.0 beta2
- Initial release

# EdenUniverseBuilder v3.0.0 beta2.1
- Fixed issue where the player was unable to get past
the loading screen

# EdenUniverseBuilder v3.0.0 beta2.2 || 16-01-2018
- Fixes issue that prevented players from loading worlds
- Optimized world loading
- Added moveable main menu UI
- Added error log
- Main Menu text + other various improvements
- Fixed issue where user could open the block menu and pause menu
at the same time
- Fixed issue where old logo was scaled incorrectly
- Fixes for main menu background bug
- Fixes for missing textures in options menu
- Main menu now loops background image forever

# EdenUniverseBuilder v3.0.0 beta2.3
- Added terrain generator for flat worlds
- Optimized world loading
- Added the ability to create and delete worlds
- Added jump button
- Added a way to view logs in-game
- Added new UI (developer mode)
- Fixed main menu background bugs
